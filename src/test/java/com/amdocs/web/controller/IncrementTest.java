package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {

    @Test
    public void testCounter() throws Exception {
        int digit = new Increment().getCounter();
        assertEquals("Error", 1 , digit);
    }
     @Test
    public void testDecreaseCounter() throws Exception {
        int digit = new Increment().decreasecounter(0);
        assertEquals("Error", 1 , digit);
    }
       @Test
    public void testDecreaseCounter1() throws Exception {
        int digit = new Increment().decreasecounter(1);
        assertEquals("Error", 1 , digit);
    }
       @Test
    public void testDecreaseCounter2() throws Exception {
        int digit = new Increment().decreasecounter(2);
        assertEquals("Error", 1 , digit);
    }

}
